import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class Pagination extends PureComponent {
    generatePagesButtons() {
        const { total, limit, selectedPage, onClickAction } = this.props;
        const pages = Math.ceil(total/limit);
        const pagesButtons = [];

        for (let i = 0; i < pages; i++) {
            const button = <button
                key={`pagination-button-${i}`}
                type="button"
                onClick={() => {
                    onClickAction(i * limit);
                }}
                disabled={selectedPage === i + 1}
            >{i+1}</button>;
            pagesButtons.push(button);
        }

        return pagesButtons;
    }
    render() {
        return (
            <nav>
                {this.generatePagesButtons()} 
            </nav>
        );
    };
}

Pagination.propTypes = {
    total: PropTypes.number.isRequired,
    limit: PropTypes.number.isRequired,
    selectedPage: PropTypes.number.isRequired,
    onClickAction: PropTypes.func.isRequired,
};

export default Pagination;
