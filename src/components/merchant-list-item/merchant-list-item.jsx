import React, { PureComponent } from 'react';
import { merchantSchema } from '../../config/proptypes';
import PropTypes from 'prop-types';

class MerchantListItem extends PureComponent {
    constructor(props) {
        super(props);
        this.onClickHandler = this.onClickHandler.bind(this);
    }

    onClickHandler() {
        const {
            merchant,
            selectMerchantAction,
        } = this.props;

        selectMerchantAction(merchant);
    }
    
    render() {
        const {
            id,
            firstname,
            lastname,
            avatarUrl,
            email,
            phone,
            hasPremium,
        } = this.props.merchant;

        return (
            <div onClick={this.onClickHandler}>
                <div>
                    <img src={avatarUrl} alt="Avatar image" />
                </div>

                <div>
                    { hasPremium && <span>*</span>}
                    <b>{`${firstname} ${lastname}`}</b>
                </div>

                <div>
                    {email}
                </div>

                <div>
                    {phone}
                </div>
            </div>
        );
    }
}

MerchantListItem.propTypes = {
    merchant: merchantSchema,
    selectMerchantAction: PropTypes.func.isRequired,
};

export default MerchantListItem;
