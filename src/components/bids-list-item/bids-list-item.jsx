import React, { PureComponent } from 'react';
import { bidSchema } from '../../config/proptypes';

class Bid extends React.PureComponent {
    render() {
        const {
            id,
            carTitle,
            amount,
            created,
        } = this.props.bid;

        return (
            <article>
                <div>({id})</div>
                <div>{created}</div>
                <div>{carTitle}</div>
                <div>{amount}</div>
            </article>
        );
    }
}

Bid.propTypes = {
    bid: bidSchema.isRequired,
};

export default Bid;
