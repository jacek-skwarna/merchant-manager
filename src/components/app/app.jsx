import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import MerchantComponent from '../merchant';
import MerchantListComponent from '../merchants-list';
import PropTypes from 'prop-types';
import { merchantSchema, bidSchema } from '../../config/proptypes';
import actions from '../../redux/actionTypes';
import { MERCHANTS_PAGE_LIMIT } from '../../config/constants';
import PaginationComponent from '../pagination';

export class App extends PureComponent {
    componentDidMount() {
        const { fetchMerchants } = this.props;
        fetchMerchants(0);
    }
    render() {
        const {
            merchants,
            loadingMerchants,
            selectedMerchant,
            total,
            page,
            fetchMerchants,
            selectMerchant,
        } = this.props;

        return (
            <Fragment>
                <header>
                    Merchant Manager
                </header>

                {selectedMerchant && <MerchantComponent merchant={selectedMerchant} />}
                {loadingMerchants && <div><span>loading merchants...</span></div>}
                {merchants.length && <MerchantListComponent
                    merchants={merchants}
                    selectMerchantAction={selectMerchant}
                />}
                {merchants.length && (
                <PaginationComponent
                    total={total}
                    limit={MERCHANTS_PAGE_LIMIT}
                    selectedPage={page}
                    onClickAction={fetchMerchants}
                />
                )}

                <footer>
                    Merchant Manager app
                </footer>
            </Fragment>
        );
    }
}

App.propTypes = {
    merchants: PropTypes.arrayOf(merchantSchema).isRequired,
    loadingMerchants: PropTypes.bool.isRequired,
    selectedMerchant: merchantSchema.isRequired,
    page: PropTypes.number.isRequired,
    total: PropTypes.number.isRequired,
    fetchMerchants: PropTypes.func.isRequired,
    selectMerchant: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
    merchants: state.merchants.merchants,
    page: state.merchants.page,
    total: state.merchants.total,
    selectedMerchant: state.merchants.selectedMerchant,
});

const mapDispatchToProps = (dispatch) => ({
    fetchMerchants: (offset) => {
        dispatch({ type: actions.FETCH_MERCHANTS, offset, limit: MERCHANTS_PAGE_LIMIT });
    },
    selectMerchant: (merchant) => {
        dispatch({ type: actions.SELECT_MERCHANT, merchant });
    },
});
  
export default connect(mapStateToProps, mapDispatchToProps)(App);
