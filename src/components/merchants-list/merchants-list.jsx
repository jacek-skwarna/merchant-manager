import React from 'react';
import PropTypes from 'prop-types';
import MerchantListItemComponent from '../merchant-list-item';
import { merchantSchema } from '../../config/proptypes';

class MerchantsList extends React.PureComponent {
    render() {
        const { merchants, selectMerchantAction } = this.props;
        return (
            <section>
                <header>Merchants</header>
                
                {
                    merchants.map((merchant) => (
                        <MerchantListItemComponent
                            merchant={merchant}
                            key={merchant.id}
                            selectMerchantAction={selectMerchantAction}
                        />
                    ))
                }

                <footer>
                    pagination
                </footer>
            </section>
        );
    }
}

MerchantsList.propTypes = {
    merchants: PropTypes.arrayOf(merchantSchema).isRequired,
    selectMerchantAction: PropTypes.func.isRequired,
};

export default MerchantsList;
