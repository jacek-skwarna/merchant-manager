import React, { PureComponent, Fragment } from 'react';
import BidsListComponent from '../bids-list';
import PropTypes from 'prop-types';
import { merchantSchema } from '../../config/proptypes';

class Merchant extends PureComponent {
    render() {
        const {
            id,
            firstname,
            lastname,
            avatarUrl,
            email,
            phone,
            hasPremium,
            bids,
        } = this.props.merchant;

        return (
            <Fragment>
                <div>
                    <div>
                        <img src={avatarUrl} alt="Avatar image" />
                    </div>

                    <div>
                        {hasPremium && <div>Premium</div>}
                        <form>
                            <fieldset>
                                <p>
                                    <input type="text" name={`firstName${id}`} id={`first_name_${id}`} value={firstname} required />
                                    <label htmlFor="first_name">First Name</label>
                                </p>
                                <p>
                                    <input type="text" name={`lastName${id}`} id={`last_name_${id}`} value={lastname} required />
                                    <label htmlFor="last_name">Last Name</label>
                                </p>
                                <p>
                                    <input type="email" name={`email${id}`} id={`email_${id}`} value={email} required />
                                    <label htmlFor="email">Email</label>
                                </p>
                                <p>
                                    <input type="text" name={`phone${id}`} id={`phone_${id}`} value={phone} required />
                                    <label htmlFor="phone">Phone</label>
                                </p>
                                <p>
                                    <input type="file" name={`avatar${id}`} id={`avatar_${id}`} />
                                    <label htmlFor="avatar">Avatar</label>
                                </p>
                            </fieldset>
                        </form>
                    </div>
                </div>

                {bids.length && <BidsListComponent bids={bids} />}
            </Fragment>
        );
    }
}

Merchant.propTypes = {
    merchant: merchantSchema.isRequired,
};

export default Merchant;