import React, { PureComponent } from 'react';
import BidsListItemComponent from '../bids-list-item';
import PropTypes from 'prop-types';
import { bidSchema } from '../../config/proptypes';

class BidsList extends PureComponent {
    generateBidsListItems() {
        const { bids } = this.props;

        return bids.map((bid) => (
            <BidsListItemComponent bid={bid} key={bid.id} />
        ));
    };

    render() {
        return (
            <section>
                <header>Bids</header>
                
                {this.generateBidsListItems()}
            </section>
        );
    };
}

BidsList.propTypes = {
    bids: PropTypes.arrayOf(bidSchema).isRequired,
};

export default BidsList;
