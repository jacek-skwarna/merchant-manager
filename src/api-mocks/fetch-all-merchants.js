import merchantsList from './all-merchants.json';

export default function fetchMerchants(offset, limit) {
    const merchantsLength = merchantsList.length;
    const merchantsToReturn = offset >= merchantsLength
    ? [] : merchantsList.slice(offset, offset + limit);

    return Promise.resolve({
        merchants: merchantsToReturn,
        total: merchantsLength,
    });
}