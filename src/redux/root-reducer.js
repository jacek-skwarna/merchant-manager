import { combineReducers } from 'redux';
import merchants from './merchants-reducers';

export const appReducer = combineReducers({
    merchants,
});

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
