import types from './actionTypes';
import { List } from 'immutable';

const defaultState = {
    loadingMerchants: false,
    merchants: [],
    selectedMerchant: null,
    fetchingMerchantsError: null,
    total: 0,
    page: 1,
};

function merchantsReducer(state = defaultState, action) {
    switch (action.type) {
        case types.FETCH_MERCHANTS:
            return ({
                ...state,
                loadingMerchants: true,
                fetchingMerchantsError: null,
            });

        case types.FETCH_MERCHANTS_SUCCESS:
            return ({
                ...state,
                loadingMerchants: false,
                merchants: action.merchants,
                total: action.total,
                page: action.page,
            });

        case types.FETCH_MERCHANTS_FAILURE:
            return ({
                ...state,
                loadingMerchants: false,
                fetchingMerchantsError: action.error,
            });

        case types.SELECT_MERCHANT:
            return ({
                ...state,
                selectedMerchant: { ...action.merchant },
            });

        default:
            return state;
    }
}

export default merchantsReducer;
