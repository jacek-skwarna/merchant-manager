import types from './actionTypes';

export const fetchMerchants = (page, limit) => (
    {
        type: types.FETCH_MERCHANTS,
        page,
        limit,
    }
);

export const fetchMerchantsSuccess = (merchants, total, page) => (
    {
        type: types.FETCH_MERCHANTS_SUCCESS,
        merchants,
    }
);

export const fetchMerchantsFailure = (error) => (
    {
        type: types.FETCH_MERCHANTS_FAILURE,
        error,
    }
);

export const addMerchant = (merchant) => (
    {
        type: types.ADD_MERCHANT,
        merchant,
    }
);

export const addMerchantSuccess = (merchant) => (
    {
        type: types.ADD_MERCHANT_SUCCESS,
        merchant,
    }
);

export const addMerchantFailure = (error) => (
    {
        type: types.ADD_MERCHANT_FAILURE,
        error,
    }
);

export const updateMerchant = (merchant) => (
    {
        type: types.UPDATE_MERCHANT,
        merchant,
    }
);

export const updateMerchantSuccess = (merchant) => (
    {
        type: types.UPDATE_MERCHANT_SUCCESS,
        merchant,
    }
);

export const updateMerchantFailure = (error) => (
    {
        type: types.UPDATE_MERCHANT_FAILURE,
        error,
    }
);

export const deleteMerchant = (merchantId) => (
    {
        type: types.DELETE_MERCHANT,
        merchantId,
    }
);

export const deleteMerchantSuccess = (merchantId) => (
    {
        type: types.DELETE_MERCHANT_SUCCESS,
        merchantId,
    }
);

export const deleteMerchantFailure = (error) => (
    {
        type: types.DELETE_MERCHANT_FAILURE,
        error,
    }
);

export const selectMerchant = (merchant) => (
    {
        type: types.SELECT_MERCHANT,
        merchant,
    }
);
