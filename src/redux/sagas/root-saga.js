import { all } from 'redux-saga/effects';
import { watchMerchantsActions } from './merchants-saga';

export default function* rootSaga() {
    yield all([
        watchMerchantsActions(),
    ]);
}