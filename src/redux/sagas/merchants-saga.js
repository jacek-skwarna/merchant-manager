import {
    put,
    takeLatest,
    call,
    select,
} from 'redux-saga/effects';
import fetchMerchantsCall from '../../api-mocks/fetch-all-merchants';
import types from '../actionTypes';

export function* fetchAllMerchants(action) {
    try {
        const { offset, limit } = action;
        const results = yield call(fetchMerchantsCall, offset, limit);
        const { merchants, total } = results;
        const page = offset >= total ? 1 : parseInt(offset / limit) + 1;
        
        yield put({
            type: types.FETCH_MERCHANTS_SUCCESS,
            merchants,
            total,
            page,
        });
    } catch (error) {
        yield put({
            type: types.FETCH_MERCHANTS_FAILURE,
            error,
        });
    }
}

export function* watchMerchantsActions() {
    yield takeLatest(types.FETCH_MERCHANTS, fetchAllMerchants);
}
