import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from './redux/store';
import AppComponent from './components/app';

ReactDOM.render(<Provider store={store}><AppComponent /></Provider>, document.querySelector("#merchant-manager-app"));
