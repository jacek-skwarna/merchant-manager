import PropTypes from 'prop-types';

export const bidSchema = PropTypes.shape({
    id: PropTypes.string.isRequired,
    carTitle: PropTypes.string.isRequired,
    amount: PropTypes.number.isRequired,
    created: PropTypes.string.isRequired,
});

export const merchantSchema = PropTypes.shape({
    id: PropTypes.string.isRequired,
    firstname: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    hasPremium: PropTypes.bool.isRequired,
    bids: PropTypes.arrayOf(bidSchema).isRequired,
});